//canvas initialization
var canvas = document.getElementById('myCanvas');
var ctx = canvas.getContext("2d");
ctx.lineWidth = 20;
var state = ctx.getImageData(0,0,canvas.width,canvas.height);
window.history.pushState(state,null);
//initial mode
var mode = "draw";
//mode type
var draw = false;
var rect = false;
var ellipse = false;
var triangle = false;
var line = false;
var erase = false;
var text = false;
var start_x,start_y;

//slider initialization
var slider = document.getElementById("brushSize");
var output = document.getElementById("demo");
output.innerHTML = slider.value; 
slider.oninput = function() {
    output.innerHTML = this.value;
    ctx.lineWidth = slider.value;
}

function Rec(){
    mode = "rect";
}

function Draw(){
    mode = "draw";
}

function Ellipse(){
    mode = "ellipse";
} 

function Tri(){
    mode = "triangle";
}

function Line(){
    mode = "line";
}

function Erase(){
    mode = "erase";
}

var mouseX;
var mouseY;
var textX;

function TEXT(){
    mode = "text";
}

function Undo(){
    window.history.go(-1);
}

function Redo(){
    window.history.go(1);
}

window.addEventListener('popstate', changeStep, false);

function changeStep(e){
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  if(e.state){
    ctx.putImageData(e.state, 0, 0);
  }
}

var startState;

function down(e){
    ctx.strokeStyle = document.getElementById("color").value;
    startState = ctx.getImageData(0,0,canvas.width,canvas.height);
    window.history.pushState(state,null);
    if(mode == "draw"){
        ctx.moveTo(e.x,e.y);
        draw = true;
        ctx.beginPath();
    }
    else if(mode == "rect"){
        //ctx.moveTo(e.x,e.y);
        start_x = e.x;
        start_y = e.y;
        rect = true;
        ctx.beginPath();
    }else if(mode == "ellipse"){
        start_x = e.x;
        start_y = e.y;
        ellipse = true;
        ctx.beginPath();
    }
    else if(mode == "triangle"){
        start_x = e.x;
        start_y = e.y;
        triangle = true;
        ctx.beginPath();
    }
    else if(mode =="line"){
        start_x = e.x;
        start_y = e.y;
        line = true;
        ctx.beginPath();
    }
    else if(mode == "erase"){
        ctx.moveTo(e.x,e.y);
        erase = true;
        ctx.clearRect((e.x-ctx.lineWidth/2)>0?e.x-ctx.lineWidth/2:0,(e.y-ctx.lineWidth/2)>0?e.y-ctx.lineWidth/2:0,ctx.lineWidth,ctx.lineWidth);
    }
    else if(mode == "text"){
        mouseX = e.x - canvas.offsetLeft;
        mouseY = e.y - canvas.offsetTop;
        textX = mouseX;
    }
}

document.addEventListener('keydown',function(e){
    ctx.font = "16px Arial"
    if(e.key === "Enter"){
        mouseX = textX;
        mouseY += 20;
    }
    else{
        ctx.fillText(e.key,mouseX,mouseY);
        mouseX += ctx.measureText(e.key).width;
    }
},false);

function move(e){
    if(mode == "draw"){
        if(draw){
            ctx.lineTo(e.x,e.y);
            ctx.stroke();
        }
    }
    else if(mode == "rect"){
        if(rect){
            //window.history.go(startState);
            ctx.rect(start_x,start_y,e.x-start_x,e.y-start_y);
            ctx.stroke();
        }
    }
    else if(mode == "ellipse"){
        if(ellipse){
            //window.history.go(-1);
            ctx.ellipse(start_x,start_y,e.x-start_x,e.y-start_y,0,0,Math.PI*2);
            ctx.stroke();
        }
    }
    else if(mode == "triangle"){
        if(triangle){
            //window.history.go(-1);
            ctx.moveTo(e.x,e.y);
            ctx.lineTo(start_x,e.y);
            ctx.lineTo((e.x+start_x)/2,start_y);
            ctx.closePath();
            ctx.stroke();
        }
    }
    else if(mode == "line"){
        if(line){
            //window.history.go(-1);
            ctx.moveTo(e.x,e.y);
            ctx.lineTo(start_x,start_y);
            ctx.closePath();
            ctx.stroke();
        }
    }
    else if(mode == "erase"){
        if(erase){
            ctx.clearRect((e.x-ctx.lineWidth/2)>0?e.x-ctx.lineWidth/2:0,(e.y-ctx.lineWidth/2)>0?e.y-ctx.lineWidth/2:0,ctx.lineWidth,ctx.lineWidth);
        }
    }
}

function up(e){
    if(mode == "draw"){
        draw = false;
        ctx.closePath();
    }
    else if(mode == "rect"){
        rect = false;
        ctx.closePath();
    }
    else if(mode == "ellipse"){
        ellipse = false;
        ctx.closePath();
    }
    else if(mode == "triangle"){
        triangle = false;
    }
    else if(mode == "line"){
        line = false;
        ctx.closePath();
    }
    else if(mode == "erase"){
        erase = false;
    }
    var state = ctx.getImageData(0,0,canvas.width,canvas.height);
    window.history.pushState(state,null);
}

document.getElementById("reset").addEventListener('click',()=>ctx.clearRect(0,0,canvas.clientWidth,canvas.height),false);
document.getElementById("download").addEventListener('click',()=>document.getElementById("dl").href = canvas.toDataURL(),false);
