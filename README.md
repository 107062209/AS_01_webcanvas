# Software Studio 2021 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | N         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | N         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | N         |


---

### How to use 

    Describe how to use your web and maybe insert images to help you explain.
	點選下列的按鈕來選取工具，在畫布上畫畫。
	左下方的調色盤可以選取顏色，旁邊的拉條可以調整筆刷大小
	pen可以畫任意形狀
	eraser可以將畫布上的東西擦除
	text可以在鼠標處打字
	triangle、circle、rectangle、line可以畫出對應的形狀
	download可以將畫布上的東西下載下來
	reset可以將畫布清空
	undo可以將回到上一個動作
	redo可以續做下一個動作

### Function description

    Decribe your bouns function and how to use it.
	

### Gitlab page link

    your web page URL, which should be "https://[studentID].gitlab.io/AS_01_WebCanvas"
	https://107062209.gitlab.io/AS_01_WebCanvas/

### Others (Optional)

    Anythinh you want to say to TAs.

<style>
table th{
    width: 100%;
}
</style>